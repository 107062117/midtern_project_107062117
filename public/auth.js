var database = firebase.database();
var storage = firebase.storage();
var email, password, username;


function sign_in(){
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    username = document.getElementById("username").value;
    /*database.ref('user/'+username).once('value', (snapshot)=>{
        if (snapshot.exists()) console.log("exists");
    })*/
    console.log("auth")
    firebase.auth().createUserWithEmailAndPassword(email,password).then(
        function(){
            var storageRef = storage.ref('default/user.png');
            storageRef.getDownloadURL().then((url)=>{
                var ref = storage.ref('default/style.jpg');
                ref.getDownloadURL().then((url2)=>{
                    //console.log(url,  url2);
                    alert("Sign in Successfully");
                    database.ref('user/'+username).set({
                        useremail:email, 
                        userpw:password, 
                        image: url, 
                        profile: "Hello! My name is " + username, 
                        styleimg:url2
                    });
                });
            });

            email.value="";
            username.value="";
            password.value="";
        }
        ,function(error) {
            alert(error.message); 
        }
    );
}

function log_in(){
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    firebase.auth().signInWithEmailAndPassword(email, password).then(
        function (){
            window.location.href="welcome.html";
            email.value="";
            password.value="";
        }
        ,function(error) {
            alert(error.message);
        }
    );
}

function google_sign_in(){
    console.log("here1")
    var provider = new firebase.auth.GoogleAuthProvider();
    var user;
    firebase.auth().signInWithPopup(provider).then(function(result) { 
        // This gives you a Google Access Token. You can use it to access the Google API. 
        var token = result.credential.accessToken;
        // The signed-in user info. 
        user = result.user;
        var ref = firebase.database().ref('user/');
        var flag = 0;
        console.log("here2")
        ref.once('value').then(function(snapshot){
            snapshot.forEach(element => {
                if (element.key === user.displayName){
                    flag = 1;
                }
            });
        }).then(()=>{
            console.log("here3")
            if (flag === 0){
                var storageRef = storage.ref('default/user.png');
                console.log()
                storageRef.getDownloadURL().then((url)=>{
                    var ref = storage.ref('default/style.jpg');
                    ref.getDownloadURL().then((url2)=>{
                        username = user.displayName;
                        console.log(user)
                        database.ref('user/'+username).set({
                            useremail:user.email, 
                            userpw:"google password", 
                            image: url, 
                            profile: "Hello! My name is " + username, 
                            styleimg:url2
                        });
                    });
                });
            }
        })
        window.location.href="welcome.html";
    }).catch(function(error) {
        // Handle Errors here. 
        var errorCode = error.code; 
        var errorMessage = error.message; 
        // The email of the user's account used. 
        var email = error.email; 
        // The firebase.auth.AuthCredential type that was used. 
        var credential = error.credential; 
        console.log(error.message)
        alert(error.message);
    });
}

function key(event){
    // if (event.keyCode===13){
    //     if (window.location.href === "http://localhost:5000/index.html"||window.location.href==="https://midtermproject107062117.web.app/"||window.location.href==="https://midtermproject107062117.web.app/index.html")
    //         sign_in();
    //     else if (window.location.href === "http://localhost:5000/login.html"||window.location.href==="https://midtermproject107062117.web.app/login.html")
    //         log_in();
    // }
}
function reset_password() {
  var auth = firebase.auth();
  var emailAddress = document.getElementById("forget_email").value;
  auth.sendPasswordResetEmail(emailAddress).then(function () {
    // Email sent.
    console.log('send!');
    alert("The email is sent to your email. Check your email to reset your password!");
  }).catch(function (error) {
    // An error happened.
    alert(error);
  });
}