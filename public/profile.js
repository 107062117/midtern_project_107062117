var storage = firebase.storage();
var database = firebase.database();
var user_email;
var username;
var isEditting;

function profile_init(){
    document.getElementById("hidden_block").style.display="none";
    isEditting = false;
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            user_email = user.email;
            var btnLogout = document.getElementById("logout");
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(result){
                alert("log out Successfully!");
            }).catch(function(error){
                    alert(error.message);
                });
            });
        } else {
            var menu = document.getElementById("navbarNav");
            menu.innerHTML="<ul class='navbar-nav'><li class='nav-item'><a class='nav-link' href='index.html' id='signin'>Sign in</a></li></ul>";
            document.getElementById("user_message").innerHTML="Chatroom";
            document.getElementById("pro").innerHTML = "";
            document.getElementById("username").innerHTML="";
            document.getElementById("img").src="";
            document.getElementById("styleimg").src="";
            window.location.href="login.html";
        }
    });
    var postsRef = database.ref('user/');
    var img = document.getElementById("img");
    var styleimg = document.getElementById("styleimg");
    postsRef.once('value')
    .then(function(snapshot) {
        snapshot.forEach(element => {
            if (element.val().useremail === user_email){
                username = element.key;
                document.getElementById("user_message").innerHTML="Hi! "+username;
                document.getElementById("username").innerHTML = "<strong>"+username+"</strong>";
            }
        });
    }).then(()=>{
        database.ref('user/'+username).once('value')
        .then(
            function(snapshot){
                img.src = snapshot.val().image;
                styleimg.src = snapshot.val().styleimg;
                console.log(snapshot.val().profile);
                document.getElementById("pro").innerHTML = snapshot.val().profile;
            }
        );
    })
}

function edit_profile(){
    var block = document.getElementById("hidden_block");
    block.style.display="initial";
    isEditting = true;
    var close = document.getElementById("close_btn");
    close.onclick = function(){
        isEditting = false;
        block.style.display = "none"
    };
}

function profile_update(){
    if (isEditting){
        var  ctn = document.getElementById("profile_input").value;
        if (ctn!=""){
            database.ref("user/"+username).update({profile: ctn});
            document.getElementById("pro").innerHTML = ctn;
            ctn.value="";
            document.getElementById("hidden_block").style.display = "none";
            isEditting = false;
        }
        else{alert("Please type something!")}
    }
}

function change_img(){
    var img = document.getElementById("img");
    var fileButton = document.getElementById('profile_img_upload');
    fileButton.click();
    fileButton.addEventListener("change", function(e) {
        var file = e.target.files[0];
        var storageRef = storage.ref(username+'/'+file.name);
        storageRef.put(file).then(function(){
            storageRef.getDownloadURL()
            .then(function(url){
                img.src = url;
                database.ref('user/'+username).update({image:url});
                fileButton.value="";
            })
            .catch((e)=>console.log(e.message));
        });
    });
}

function change_styleimg(){
    var img = document.getElementById("styleimg");
    var fileButton = document.getElementById('style_img_upload');
    fileButton.click();
    fileButton.addEventListener("change", function(e) {
        var file = e.target.files[0];
        var storageRef = storage.ref(username+'/'+file.name);
        storageRef.put(file).then(function(){
            storageRef.getDownloadURL()
            .then(function(url){
                img.src = url;
                database.ref('user/'+username).update({styleimg:url});
                fileButton.value="";
            })
            .catch((e)=>console.log(e.message));
        });
    });
}

function key(event){
    if (isEditting){
        if (event.keyCode===13) profile_update();
    }
}