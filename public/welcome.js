var user_array = [];
var database=firebase.database();
var user_email = '';
var user_key;
var chatroom, friend;
var username;
var isFriendroom = false, isChatroom = false;
var isChatting = false, isWelcome = true;

function welcome_init(){
    notification();
    isWelcome=true;
    document.getElementById("chatroom").style.display="none";
    document.getElementById("welcome").style.display="initial";
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            user_email = user.email;
            var btnLogout = document.getElementById("logout");
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(result){
                    alert("log out Successfully!");
            }).catch(function(error){
                    alert(error.message);
                });
            });
        } else {
            var menu = document.getElementById("navbarNav");
            menu.innerHTML="<ul class='navbar-nav'><li class='nav-item'><a class='nav-link' href='index.html' id='signin'>Sign in</a></li></ul>";
            document.getElementById("user_message").innerHTML="Chatroom";
            document.getElementById("show_chosed_users").innerHTML="";
            document.getElementById("show_chatroom").innerHTML="";
            document.getElementById("show").innerHTML="";
            document.getElementById("current_chatroom").innerHTML="";
            window.location.href="login.html";
            //var div = document.getElementById("show_member");
            document.getElementById("show_friends")="";
        }
    });
    //username
    
    var postsRef = database.ref('user/');
    postsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(element => {
                //console.log(element.val().useremail);
                if (element.val().useremail === user_email){
                    username = element.key;
                    document.getElementById("user_message").innerHTML="Hi! "+username;
                }
            });
        })
        .then(()=>{//create chatroom_btn
            var postChatRoomRef = database.ref('user/' + username+ '/chatroom');
            var chatroom_first = 0;
            var chatroom_second = 0;
            postChatRoomRef.once('value')
            .then(function(snapshot){
                snapshot.forEach(element => {
                    chatroom_first += 1;
                    create_chatroom_btn(element.key);
                    console.log("!");
                });
                
                //Update new chatroom
                postChatRoomRef.on('child_added',function(element){
                    chatroom_second += 1;
                    if (chatroom_first < chatroom_second){
                        create_chatroom_btn(element.key);
                        update_chatroom(element.key);
                        console.log("@");
                    };
                });
            });

            //create friend_btn
            var ref = database.ref('user/'+username+'/friend');
            var friend_first = 0;
            var friend_second = 0;
            ref.once('value')
            .then(function(snapshot){
                snapshot.forEach(element => {
                    friend_first += 1;
                    create_friend_btn(element.key);
                });
                //Update new friend
                ref.on('child_added', function(element){
                    friend_second += 1;
                    console.log(element);
                    console.log(element.val());
                    if (friend_first < friend_second){
                        console.log(friend_first, friend_second)
                        create_friend_btn(element.key);
                        update_friend(element.key);
                    }
                });
            })
        })
        .catch(e => {
            console.log(e.message)
        });
}
///////////////////////////////////////////////////////////////////////////////
function find_user(){
    document.getElementById("show_result").innerHTML="";
    var user = document.getElementById("user").value;
    var ref = database.ref('user/');
    var flag = 0;
    if (user === username) {
        user.value="";
        alert("It is yourself");
        return;
    }
    ref.once('value', function(snapshot){
        snapshot.forEach(element => {
            if (element.key === user){
                flag = 1;
            }
        });
    }).then(()=>{
        if (flag === 0){
            user="";
            alert("Not found!");
        } 
        else choose_friend(user);
    });
}

function choose_friend(user){
    var show = document.getElementById("show_result");
    var div = document.createElement("div");
    var btn = document.createElement("input");
    var img = document.createElement("img");
    img.classList.add("img");
    database.ref('user/'+user).once('value')
    .then(function(snapshot){
        img.src = snapshot.val().image;
    }).then(()=>{
        btn.type = "button";
        btn.value = user;
        btn.classList.add("btn");
        div.classList.add("friend");
        div.appendChild(img);
        div.appendChild(btn);
        show.appendChild(div);
        document.getElementById("user").value="";
        btn.addEventListener('click', () => add_friend(user));
    });
}

function add_friend(user){
    var flag = 0;
    var ref = database.ref('user/'+username+'/friend/');
    ref.once('value')
    .then(function(snapshot){
        snapshot.forEach((element)=>{
            if (element.key === user) flag = 1;
        });
    }).then(()=>{
        if (flag === 1) alert("You're friends already.");
        else{
            ref = database.ref('user/'+username+'/friend/'+user);
            ref.set({default:1});
            ref = database.ref('user/'+user+'/friend/'+username);
            ref.set({default:1});
            document.getElementById("show_result").innerHTML="";
            //create_friend_btn(user);
        }
    })
}

function create_friend_btn(element){
    var show = document.getElementById("show_friends");
    var div = document.createElement("div");
    var btn = document.createElement("input");
    var img = document.createElement("img");
    img.classList.add("img");
    img.style.cursor = "pointer";
    img.addEventListener('click', ()=>show_friend_pofile(element));
    database.ref('user/'+element).once('value')
    .then(function(snapshot){
        img.src = snapshot.val().image;
    }).then(()=>{
        btn.type = "button";
        btn.id = element;
        btn.value = element;
        btn.classList.add("btn");
        div.classList.add("friend");
        div.appendChild(img);
        div.appendChild(btn);
        show.appendChild(div);
        document.getElementById("user").value="";
        btn.addEventListener('click', ()=>choose_friendroom(btn.id));
    });
}

function choose_friendroom(id){
    friend = id;
    isFriendroom = true;
    document.getElementById("welcome").style.display="none";
    document.getElementById("chatroom").style.display="initial";
    chatroom_init();
}

function show_friend_pofile(element){
    var modal = document.getElementById("friend_profile_container");
    var span = document.getElementById("profile_close");

    modal.style.display="block";
    span.addEventListener('click', ()=>{
        modal.style.display = "none";
    });
    
    var img = document.getElementById("img");
    var styleimg = document.getElementById("styleimg");
    var p = document.getElementById("p");
    database.ref('user/'+element).once('value')
    .then((snapshot)=>{
        img.src=snapshot.val().image;
        styleimg.src=snapshot.val().styleimg;
        p.innerHTML=snapshot.val().profile;
    });
}

/////////////////////////////////////////////////////////////////////////////////
function add_member(){
    var member = document.getElementById("member").value;
    var ref = database.ref('user/'+username+'/friend');
    var flag = 0;
    ref.once('value')
    .then(function(snapshot){
        snapshot.forEach(element => {
            if (element.key === member){
                flag = 1;
                for (var i = 0; i < user_array.length; i++){
                    if (user_array[i] === member){
                        flag = 0;
                        break;
                    }
                }
            }
        });
    }).then(()=>{
        if (flag === 0) {
            alert("Please add "+member+" to your friend first!");
        }
        else choose_member(member);
    });
}

function choose_member(id){
    user_array.push(id);
    var show = document.getElementById("show_chosed_users");
    var btn = document.createElement("input");
    var div = document.createElement("div");
    var img = document.createElement("img");
    img.classList.add("img");
    database.ref('user/'+id).once('value')
    .then(function(snapshot){
        img.src = snapshot.val().image;
    }).then(()=>{
        btn.type = "button";
        btn.value = id;
        btn.classList.add("btn");
        div.classList.add("friend");
        div.appendChild(img);
        div.appendChild(btn);
        show.appendChild(div);
        document.getElementById("member").value="";
    });
}

function createRoom(){
    var roomname = document.getElementById("chatroom_name").value;
    if (roomname === ""){
        alert("Please enter Chatroom name.");
        return;
    }
    var ref = database.ref('chatroom/'+roomname);
    ref.once('value', (snapshot)=>{
        if(snapshot.exists()){
            alert("The chatroom had existed. Tyr another!");
        }
        else{
            for (i = 0; i < user_array.length ; i += 1){
                database.ref('user/'+user_array[i]+'/chatroom/'+roomname).set({default:"Hellow"});
                database.ref('chatroom/'+roomname+'/member').push(user_array[i]);
            }
            database.ref('chatroom/'+roomname+'/member').push(username);
            database.ref('user/'+username+'/chatroom/'+roomname).set({default:"Hello"});
            document.getElementById("chatroom_name").value="";
            document.getElementById("show_chosed_users").innerHTML="";
            user_array = [];
            //create_chatroom_btn(roomname);
        }
    })
}

function create_chatroom_btn(element){
    var show = document.getElementById('show_chatroom');
    var btn = document.createElement("input");
    btn.type = "button";
    btn.id = element;
    btn.value = element;
    btn.classList.add("btn");
    show.appendChild(btn);
    btn.addEventListener('click', function(){choose_chatroom(btn.value)});
}

function choose_chatroom(room){
    chatroom = room;
    isChatroom = true;
    document.getElementById("welcome").style.display="none";
    document.getElementById("chatroom").style.display="initial";
    var ref = database.ref('chatroom/'+room+'/member');
    ref.once('value')
        .then(function(snapshot) {
            snapshot.forEach(element => {
                user_array.push(element.val());
            });
        })
        .then(()=>chatroom_init())
        .catch(e => console.log(e.message));
}
//////////////////////////////////////////////////////////////////////////////////
function chatroom_init(){
    isChatting = true;
    create_memberprofile();
    if (isFriendroom)
        document.getElementById("current_chatroom").innerHTML = friend;
    else
        document.getElementById("current_chatroom").innerHTML = chatroom;
    var post_txt = document.getElementById("text_input");
    var post_btn = document.getElementById("post");
    var post_sticker = document.getElementById("sticker");

    post_sticker.addEventListener('click', function(){
        var get_time = time();
        if (isChatroom)
            database.ref('chatroom/'+chatroom+'/message').push({name:username, data:post_txt.value, time: get_time, type:"sticker"});
        else if(isFriendroom){
            database.ref('user/'+username+'/friend/'+friend+'/message').push({name:username, data:post_txt.value, time: get_time, type:"sticker"});
            database.ref('user/'+friend+'/friend/'+username+'/message').push({name:username, data:post_txt.value, time: get_time, type:"sticker"});
        }
        else console.log("get wrong");
    });

    post_btn.addEventListener('click', function(){
        if (post_txt.value != "") {
            var get_time = time();
            if (isChatroom)
                database.ref('chatroom/'+chatroom+'/message').push({name:username, data:post_txt.value, time: get_time ,type:"text"});
            else if(isFriendroom){
                database.ref('user/'+username+'/friend/'+friend+'/message').push({name:username, data:post_txt.value, time: get_time, type:"text"});
                database.ref('user/'+friend+'/friend/'+username+'/message').push({name:username, data:post_txt.value, time: get_time, type:"text"});
            }
            else console.log("get wrong")
            post_txt.value="";
        }
        else{
            alert("Please text again");
        }
    });

    var str_before_username = "<div class='show_text fadeInUp'><div class='talker'><h5><strong>";
    var str_before_username_own="<div class='show_text_own fadeInUp'><div class='talker_own'><h5><strong>"
    var str_1 = "</strong></h5></div><div class='message'><p>";
    var str_2 = "</p></div><div class='time'><p>";
    var str_after_content = "</p></div></div></div>\n";

    var postsRef;
    if (isChatroom)
        postsRef = database.ref('chatroom/'+chatroom+'/message')
    else
        postsRef = database.ref('user/'+username+'/friend/'+friend+'/message');

    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            var show = document.getElementById('show');
            var str;
            snapshot.forEach(element => {
                first_count += 1;
                if (element.val().type === "sticker")
                    str = "<img src ='sticker.png' class='sticker'/>"
                else{
                    str = htmlEntities(element.val().data);
                }
                if (username === element.val().name)
                    total_post.push(str_before_username_own + element.val().name +str_1 +str+str_2+ element.val().time + str_after_content);
                else
                    total_post.push(str_before_username + element.val().name +str_1 +str+str_2+ element.val().time + str_after_content);
            });
            show.innerHTML = total_post.join('');
            show.scrollTop = show.scrollHeight - show.clientHeight;
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var element = data.val();
                    var str;
                    if (element.type === "sticker")
                        str = "<img src ='sticker.png' class='sticker'>"
                    else{
                        str = htmlEntities(element.data);
                    };
                    if (username === element.name)
                        total_post[total_post.length] = str_before_username_own + element.name +str_1+str +str_2+ element.time + str_after_content;
                    else
                        total_post[total_post.length] = str_before_username + element.name +str_1+str +str_2+ element.time + str_after_content;
                    show.innerHTML = total_post.join('');
                    show.scrollTop = show.scrollHeight - show.clientHeight;
                    update_message(isChatroom, isFriendroom, chatroom, element.name, str, element.type);
                }
            });
        })
        .catch(e => console.log(e.message));
}

function create_memberprofile(){
    
    if (isChatroom){
        document.getElementById("member_part").innerHTML = 
            `<input id="add_chatroom_member_btn" type="button" value="Add +" onclick="add_chatroom_member()"/>
            <div id="myModal" class="modal">
            <!-- Modal content -->
                <div class="modal-content" id="modalcnt_in_chatroom">
                    <div>
                        <span class="close" id="chatroom_close">&times;</span>
                        <h4 calss="subtitle">Add Friends here!</h4>
                    </div>
                    <p>Find user below and click to add !</p>
                    <div class="input_btn">
                        <input type="text" id="add_user" value="" placeholder=" Add Friend"/>
                        <button id="add_friend_btn" onclick="find_added_user()"></button>
                        <div id="add_show_result"></div>
                    </div>
                </div>
            </div>
            <div id="show_member"></div>`;
        var element = document.createElement('div');
        element.classList.add("member_profile");
        var ref = database.ref('user/' + username);
        var img;
        ref.once('value').then(function(snapshot){img = snapshot.val().image;})
        .then(function(){element.innerHTML="<img class='img' src='"+img+"'><p class='profile_ctx'><strong>"+username+"</strong></p>";})
        document.getElementById("show_member").appendChild(element);
        user_array.forEach((name)=>{
            if (name != username){
                var element = document.createElement('div');
                element.classList.add("member_profile");
                var ref = database.ref('user/' + name);
                var img;
                ref.once('value').then(function(snapshot){img = snapshot.val().image;})
                .then(function(){
                    element.innerHTML="<img class='img' src='"+ img +"'><p class='profile_ctx'><strong>"+name+"</strong></p>";
                });
                document.getElementById("show_member").appendChild(element);
            }
        });
    }
    else{
        //debug
        document.getElementById("member_part").innerHTML = "<div id='show_member'></div>";
        var element = document.createElement('div');
        element.classList.add("member_profile");
        var img;
        var ref = database.ref('user/'+username);
        ref.once('value').then(function(snapshot){img = snapshot.val().image;})
        .then(function(){
            element.innerHTML="<img class='img' src='"+img+"'><p class='profile_ctx'><strong>"+username+"</strong></p>";
        });
        document.getElementById("show_member").appendChild(element);
        var element2 = document.createElement('div');
        element2.classList.add("member_profile");
        ref = database.ref('user/' + friend);
        ref.once('value').then(function(snapshot){img = snapshot.val().image;})
        .then(function(){element2.innerHTML="<img class='img' src='"+img+"'><p class='profile_ctx'><strong>"+friend+"</strong></p>";})
        document.getElementById("show_member").appendChild(element2);
    }
}

function time(){
    var date = new Date;
    var month = date.getMonth()+1;
    if (month < 10) month = "0"+month;
    var day = date.getDate();
    if (day < 10) day = "0" + day;
    var hour = date.getHours();
    if (hour< 10) hour="0"+hour;
    var min=date.getMinutes();
    if(min<10) min="0"+min;
    var sec = date.getSeconds();
    if (sec < 10) sec="0"+sec;
    var str = month + "/" + day + " "+ hour+":"+min+":"+sec;
    return str;
}

function key(event){
    var post_txt = document.getElementById("text_input");
    if (event.keyCode===13){
        if (isChatting){
            if (post_txt.value != "") {
                var get_time = time();
                if (isChatroom)
                    database.ref('chatroom/'+chatroom+'/message').push({name:username, data:post_txt.value, time: get_time});
                else if (isFriendroom){
                    database.ref('user/'+username+'/friend/'+friend+'/message').push({name:username, data:post_txt.value, time: get_time});
                    database.ref('user/'+friend+'/friend/'+username+'/message').push({name:username, data:post_txt.value, time: get_time});
                }
                post_txt.value="";
            }
            else{
                alert("Please text again");
            };
        }
    }
}

function htmlEntities(str){
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/////////////////////////////////////////////////////////
function find_added_user(){
    var user = document.getElementById("add_user").value;
    var ref = database.ref('user/'+username+'/friend');
    var flag = 0;
    if (user === username) {
        user.value="";
        alert("It is yourself");
        return;
    }
    ref.once('value')
    .then(function(snapshot){
        snapshot.forEach(element => {
            if (element.key === user){
                flag = 1;
                for (var i = 0; i < user_array.length; i++){
                    if (user_array[i] === user){
                        flag = 0;
                        break;
                    }
                }
            }
        });
    }).then(()=>{
        if (flag === 0){
            user="";
            alert("Not friend yet or User had been in chatroom already");
        } 
        else choose_user_inchatroom(user);
    });
}

function choose_user_inchatroom(user){
    var show = document.getElementById("add_show_result");
    var btn = document.createElement("input");
    btn.type = "button";
    btn.value = user;
    btn.classList.add("btn");
    show.appendChild(btn);
    document.getElementById("add_user").value="";
    btn.addEventListener('click', ()=>add_user_inchatroom(user));
    //alert("!");
}

function add_user_inchatroom(user){
    var ref = database.ref('chatroom/'+chatroom+'/member');
    ref.push(user);
    ref = database.ref('user/'+user+'/chatroom/'+chatroom);
    ref.set({default:"Hello"});
    var element = document.createElement('div');
    element.classList.add("member_profile");
    var img;
    var userRef = database.ref('user/'+user);
    userRef.once('value').then(function(snapshot){img = snapshot.val().image;})
    .then(()=>{
        element.innerHTML= "<img class='img' src='"+img+"'/><p class='profile_ctx'><strong>"+user+"</strong></p>";
    });
    
    document.getElementById("show_member").appendChild(element);
    document.getElementById("add_show_result").innerHTML="";
}

function add_chatroom_member(){
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("add_chatroom_member_btn");
    var span = document.getElementById("chatroom_close");
    btn.onclick = function() {
        document.getElementById("modalcnt_in_chatroom").classList.add("fadeInDown");
        modal.style.display = "block";
    }
    span.onclick = function() {
        modal.style.display = "none";
        document.getElementById('add_show_result').innerHTML = "";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            document.getElementById('add_show_result').innerHTML = "";
        }
    }
}
/**********************************/
function notification(){
    if (Notification && Notification.permission !== "granted") {
        Notification.requestPermission(function (status) {
            console.log(status);
            if (Notification.permission !== status) {
                Notification.permission = status;
                console.log(status);
            }
        });
    }
    //else{
        //if (Notification && Notification.permission === "granted") {
            //var n = new Notification("Permission granted!");
        //}
    //}
}

function update_chatroom(element){
    if (Notification && Notification.permission === "granted") {
        var n = new Notification("You're added to Chatroom: "+element);
    }
}

function update_friend(element){
    if (element!=username){
        if (Notification && Notification.permission === "granted") {
            var n = new Notification(element+" add you to his/her friend");
        }
    }
    
}

function update_message(ch, fri, chname, name, str, type){
    if (Notification && Notification.permission === "granted") {
        if (type==="sticker"){
            if (username!=name)
            if (ch === true) var n = new Notification(chname+" : "+ name+" send a sticker");
            else var n = new Notification(name+" send a sticker");
        }
        else{
            if (username!=name){
                if (ch === true) var n = new Notification(chname+" :\n"+ name +" : "+str);
                else var n = new Notification(name+" : "+str);
            }
            
        }
    }
}