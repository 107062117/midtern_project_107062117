# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : MidtermProject107062117
* Key functions (add/delete)
    1. Sign up
    2. Sign in (email and Google)
    3. Private Chatroom(one to one)
    4. Chatroom (multiple people)
    5. Chrome notification
    
* Other functions (add/delete)
    1. Forget password
    2. Personal profile page
	3. Add friend
    4. Create Chatroom
    5. Check others' profile
    6. Add new members to Chatroom
    7. Send sticker in chatroom


## Website Detail Description

# 作品網址：
https://test-2-ea4e1.web.app/

# Components Description : 

1. Sign up : `以auth().createUserWithEamilAndPassword實作，每次創建的新帳號會擁有自己的username、userpassword、useremail、default的用戶頭像、用戶封面照。以及個人簡介`

2. Sign in : `以auth().signInWithEmailAndPassword實作，輸入email以及password進行登入。`

3. Google Sign In : `以auth().signInWithApp實作，第一次使用google登入的使用者會擁有google本來的username、useremail、userpassword，以及default的用戶頭像及封面照。`

4. Private Chatroom : `目前的好友會顯示在Friend區塊，點擊即可進入聊天室，進入聊天室後可於畫面左方查看目前聊天室成員。`

5. Chatroom : `點擊目標Chatroom即可進入聊天室群組開始聊天，進入聊天室後可於畫面左方查看目前聊天室成員。`

6. Chrome notification : `在被加為好友、被加入聊天室、或有人傳送訊息至聊天室時，傳送通知。`


# Other Functions Description : 
1. Forget password : `以auth.sendPasswordResetEmail()進行實作，忘記密碼時填入email並按送出即可經由該email收取更改密碼的信件。`

2. Personal profile page : `進入profile頁即可看到default的用戶頭像、用戶封面照及個人介紹，點擊可更換用戶頭像、封面照，點及個人介紹下方的編輯鈕可以對個人介紹進行編輯。`

3. Add friend : `輸入username對以所有使用者進行索引，找到的結果會顯示在下方，點擊即可加入好友。`

4. Create Chatroom : `可與眾多好友共同建立一個聊天室，已加入的聊天室會顯示在Chatroom區塊。`

5. Check others' profile : `在聊天室首頁點及好友頭像可觀看好友頭像、封面照及介紹`

6. Add new members to chatroom : `在多人聊天室中點擊左上角Add按鈕可以在彈出式方格中尋找好友加入群組。`

7. Send sticker in chatroom : `除了一般的訊息之外，亦可傳送小貼圖到聊天室中。`


## Security Report (Optional)
除了已登入的使用者外，其他人不可讀寫後端內容，包含用戶資料，聊天室內容等等。